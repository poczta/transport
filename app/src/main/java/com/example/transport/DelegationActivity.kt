package com.example.transport

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import android.widget.RadioGroup

class DelegationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delegation)

        val delegations = findViewById<RadioGroup>(R.id.Delegations)
        val delegationOdometr = findViewById<EditText>(R.id.delegationOdometr)
        val delegationRegistration = findViewById<EditText>(R.id.delegationRegistration)
        val btnSubmit = findViewById<Button>(R.id.btn_submit)

        val pref = getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
        val prefUserId = pref.getString("UserId", "")
        val registratioNumber = GetRegistration.checkRegistration(prefUserId)
             delegationRegistration.setText(registratioNumber)


        val btnBack = findViewById<Button>(R.id.btn_back)
        btnBack.setOnClickListener {
            super.onBackPressed()

        }

        btnSubmit.setOnClickListener {
            val odometr: String= delegationOdometr.text.toString()
            val registration: String = delegationRegistration.text.toString()
            val id = delegations.checkedRadioButtonId
            var radioSelect = 0
            if (id == 2131296686 ) {radioSelect = 3} else if (id ==2131296452 ){radioSelect = 2}
            Log.d("TAG==cost amount", id.toString())

             addingFunction(odometr, registration, radioSelect, prefUserId)
        }
    }

    private fun addingFunction(Odometr: String, registration: String, radioSelect: Int, prefUserId: String?) {
        val response = khttp.post(
            url = "https://ekolives.com/app-rest/adddelegation.php",
            data = mapOf(
                "userid" to prefUserId,
                "registration" to registration,
                "km" to Odometr,
                "type" to radioSelect ,
                "secret" to "dupa1"
            )
        )

        when (response.text) {
            "1" -> {
                val responsetxt = getString(R.string.addSusscess)
                Toast.makeText(this@DelegationActivity, responsetxt, Toast.LENGTH_LONG).show()
                super.onBackPressed()
            }
            "2" -> {
                val responsetxt = getString(R.string.dbError)
                Toast.makeText(this@DelegationActivity, responsetxt, Toast.LENGTH_LONG).show()
            }
            "3" -> {
                val responsetxt = getString(R.string.addError)
                Toast.makeText(this@DelegationActivity, responsetxt, Toast.LENGTH_LONG).show()
            }
        }
    }
}