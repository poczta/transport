package com.example.transport

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val etUserName = findViewById<EditText>(R.id.prof_user_name)
        val etPassword = findViewById<EditText>(R.id.prof_password)
        val btnSubmit = findViewById<Button>(R.id.prof_btn_submit)
        val btnToLogin = findViewById<Button>(R.id.profile_btn_logOut)

        btnToLogin.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        etUserName.setOnClickListener {
            Log.d("TAG", "et_user_name")
        }

        etPassword.setOnClickListener {
            Log.d("TAG", " et_password")
        }

        btnSubmit.setOnClickListener {
            Log.d("TAG", " btn_submit")
        }

    }
}

