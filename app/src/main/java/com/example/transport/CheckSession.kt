package com.example.transport

import android.content.Context
import android.util.Log
import android.content.SharedPreferences


class SharedPref {
    fun init(context: Context): Int {
        val prefs: SharedPreferences = context.getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
        val lanSettings = prefs.getString("UserId", null)

        if (lanSettings == null) {

            Log.d("TAG-from-Check--TXT", "Brak zalogowanego usera")

            return 0

        } else {

            Log.d("TAG-from-Check--TXT", "User jest zalogowany")
            return 1
        }
    }
}

