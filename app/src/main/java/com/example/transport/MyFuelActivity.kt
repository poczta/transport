package com.example.transport

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import khttp.responses.Response
import org.json.JSONObject
import android.widget.TextView as TextView



class MyFuelActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_fuel)
        getFuelList(checkLoginSessin().toString()) // get the fuel list for logged user



        val btnBack = findViewById<Button>(R.id.btn_back)
        btnBack.setOnClickListener {
            super.onBackPressed()
        }
    }


    private fun getFuelList(prefUserId: String) {

        val response: Response = khttp.get("https://ekolives.com/app-rest/MyFuel.php", data = mapOf("userid" to prefUserId))
        val getJSONArray = response.jsonArray


        for (i in 0 until getJSONArray.length()) {
            val jsonobject: JSONObject = getJSONArray.getJSONObject(i)
            val fuelsysid = jsonobject.getString("sysid")
            val fuelkm = jsonobject.getString("km")
            val fuelvolume = jsonobject.getString("volume")
            val fuelldate = jsonobject.getString("date")
            val fuelusername = jsonobject.getString("username")
            val fueluserid = jsonobject.getString("userid")
            val fuellocation = jsonobject.getString("location")
            val fueltype = jsonobject.getString("type")
            val fuelregistration = jsonobject.getString("registration")

           // Log.d("TAG==fuelsysid", fuelsysid.toString())



            val top1 = findViewById<TextView>(R.id.top1)
        val center1 = findViewById<TextView>(R.id.center1)
        val bottom1 = findViewById<TextView>(R.id.bottom1)

            top1.text = (getString(R.string.date) +": " + fuelldate)
            center1.text = (getString(R.string.fuel_odometer) +": " + fuelkm)
            bottom1.text = (getString(R.string.fuel_litersAmount) +": " + fuelvolume)


        }
    }


    private fun checkLoginSessin(): Any {
        val checkloginsessionifok = SharedPref().init(applicationContext)
        return if (checkloginsessionifok == 1) {
            val pref = getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
            val fuelUserId = pref.getString("UserId", "")
            return fuelUserId.toString()
        } else {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}

