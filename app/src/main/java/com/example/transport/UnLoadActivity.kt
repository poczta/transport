package com.example.transport

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class UnLoadActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_un_load)

        val btnBack = findViewById<Button>(R.id.btn_back)
        btnBack.setOnClickListener {
        super.onBackPressed()
    }
    }
}