package com.example.transport

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.transport.R.id


class StartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        checkLoginSessin()


        val btnEditProfile = findViewById<Button>(id.butEditProfile)
        val btnDaily = findViewById<Button>(id.btnDaily)
        val btnVacations = findViewById<Button>(id.btnVacations)
        val btnDelegation = findViewById<Button>(id.btnDelegation)
        val btnWork = findViewById<Button>(id.btnWork)
        val btnFuel = findViewById<Button>(id.btnFuel)
        val btnCosts = findViewById<Button>(id.btnCosts)

// ================ BUTONS ========== START ================
        btnDaily.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, DailyActivity::class.java)
            startActivity(intent)
        }

        btnDelegation.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, DelegationActivity::class.java)
            startActivity(intent)
        }

        btnWork.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, WorkActivity::class.java)
            startActivity(intent)
        }

        btnCosts.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, CostsActivity::class.java)
            startActivity(intent)
        }

        btnFuel.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, FuelActivity::class.java)
            startActivity(intent)
        }

        btnVacations.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, VacationsActivity::class.java)
            startActivity(intent)
        }

        btnEditProfile.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }
        // ================ BUTONS ========== END  ================

    }

    fun checkLoginSessin() {
        val checkloginsessionifok = SharedPref().init(applicationContext)
        if (checkloginsessionifok == 1) {
            val pref = getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
            val prefUserName = pref.getString("username", "")
            val textView = findViewById<TextView>(id.StartUsernameText)
            textView.text = (prefUserName)
            // Log.d("TAG==check-from-Start", checkloginsessionifok.toString())
        } else {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
