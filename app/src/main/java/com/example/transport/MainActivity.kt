package com.example.transport

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import khttp.responses.Response
import org.json.JSONObject



class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        checkLoginSessinNew()

        val etUserName = findViewById<EditText>(R.id.et_user_name)
        val etPassword = findViewById<EditText>(R.id.et_password)
        val btnReset = findViewById<Button>(R.id.btn_reset)
        val btnSubmit = findViewById<Button>(R.id.btn_submit)
        val btnToRegister = findViewById<Button>(R.id.btn_to_register)

        //------------------------- Butons sections Start ------------
        btnReset.setOnClickListener {
            etUserName.setText("")
            etPassword.setText("")
        }

        btnSubmit.setOnClickListener {
            val userName = etUserName.text
            val password = etPassword.text
            checkHost(userName.toString(), password.toString())
        }

        btnToRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
        //------------------------- Butons sections End ------------
    }

    //------------------------- Functions sections Start ------------
    private fun checkHost(user_name: String, password: String) {
        val checkhostrunning = CheckHost.checkHostAvailable()
        if (checkhostrunning != 1) {
            Toast.makeText(
                this@MainActivity,
                getString(R.string.serverOFF),
                Toast.LENGTH_LONG
            ).show()
            onPause()
        } else {
            loginFunction(user_name, password)
        }
    }

    fun checkLoginSessinNew() {

        val checkloginsessionifok = SharedPref().init(applicationContext)
        // Log.d("TAG==check-from-Main", checkloginsessionifok.toString())
        if (checkloginsessionifok == 1) {
            val intent = Intent(this@MainActivity, StartActivity::class.java)
            startActivity(intent)
        }
    }

    fun loginFunction(username: String, password: String) {
        // prod HOST "http://aatransport.pl/app-rest/login.php"
        val response: Response = khttp.get(
            "https://ekolives.com/app-rest/login.php",
            data = mapOf("username" to username, "password" to password)
        )
        val obj: JSONObject = response.jsonObject
        val iferror = obj.getString("error")
        if (iferror == "true") {

            val message = obj.getString("message")
            // Log.d("TAG", iferror.toString())
            // Log.d("TAG", message.toString())
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()

        } else {
            val getuserJSON = obj.getJSONObject("user")
            val getUserLogin = getuserJSON.getString("UserLogin")
            val getUserEmail = getuserJSON.getString("UserEmail")
            val getUserPhone = getuserJSON.getString("UserPhone")
            val getUserAdmin = getuserJSON.getString("UserAdmin")
            val getUserId = getuserJSON.getString("UserId")


            val token = getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
            val editor = token.edit()
            editor.putString("username", getUserLogin)
            editor.putString("UserEmail", getUserEmail)
            editor.putString("UserPhone", getUserPhone)
            editor.putString("UserAdmin", getUserAdmin)
            editor.putString("UserId", getUserId)
            editor.apply()


            Toast.makeText(this@MainActivity, getString(R.string.LoginOKtxt), Toast.LENGTH_SHORT)
                .show()
            val intent = Intent(this@MainActivity, StartActivity::class.java)
            intent.putExtra("Username", getUserLogin)
            startActivity(intent)

        }
    }
    //------------------------- Functions sections End ------------

}



