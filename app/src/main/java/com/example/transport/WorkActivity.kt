package com.example.transport

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class WorkActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work)

        val btnBack = findViewById<Button>(R.id.btn_back)
        val btnLoad = findViewById<Button>(R.id.btnLoad)
        val btnUnLoad = findViewById<Button>(R.id.btnUnLoad)

        btnBack.setOnClickListener {
            super.onBackPressed()
        }

        btnLoad.setOnClickListener {
            val intent = Intent(this, LoadActivity::class.java)
            startActivity(intent)
        }

        btnUnLoad.setOnClickListener {
            val intent = Intent(this, UnLoadActivity::class.java)
            startActivity(intent)
        }

    }
}