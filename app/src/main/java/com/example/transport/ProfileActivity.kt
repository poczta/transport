package com.example.transport

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        val btnEditProfile = findViewById<Button>(R.id.butLogOut)
        val btnBack = findViewById<Button>(R.id.btn_back)

        btnBack.setOnClickListener {
            super.onBackPressed()
        }

        btnEditProfile.setOnClickListener {
            val preferenceData = getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
            preferenceData.edit().remove("UserId").apply()
            preferenceData.edit().remove("username").apply()
            preferenceData.edit().remove("UserEmail").apply()
            preferenceData.edit().remove("UserPhone").apply()
            preferenceData.edit().remove("UserAdmin").apply()

            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this@ProfileActivity.finish()
        }
    }
}


