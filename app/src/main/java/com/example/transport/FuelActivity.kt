package com.example.transport

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.text.DecimalFormat

class FuelActivity : AppCompatActivity(), LocationListener {
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fuel)
        checkLoginSessin()

        val btnGetLocal = findViewById<Button>(R.id.reg_btn_local)
        val btnSubmit = findViewById<Button>(R.id.prof_btn_submit)
        val btnBack = findViewById<Button>(R.id.btn_back)
        val btnFuelList = findViewById<Button>(R.id.btn_fuelList)
        val odometer = findViewById<EditText>(R.id.fuel_odometer)
        val fuelAmount = findViewById<EditText>(R.id.fuel_litersAmount)
        val vehicleregistration = findViewById<EditText>(R.id.fuel_vehicleregistration)
        val fuelType = findViewById<RadioGroup>(R.id.FuelType)
        val pref = getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
        val prefUserId = pref.getString("UserId", "")
        val textlongitude = findViewById<TextView>(R.id.latitude)
        val textlatitude = findViewById<TextView>(R.id.longitude)

        val registratioNumber = GetRegistration.checkRegistration(prefUserId)
        vehicleregistration.setText(registratioNumber)


        btnFuelList.setOnClickListener {
            checkLoginSessin()
            val intent = Intent(this, MyFuelActivity::class.java)
            startActivity(intent)
        }

        btnGetLocal.setOnClickListener {
            getLocation()
        }

        btnSubmit.setOnClickListener {
            checkLoginSessin()

            val odometr = odometer.text.toString()
            val fuel = fuelAmount.text.toString()
            val addlongitude: String = textlongitude.text.toString()
            val addlatitude: String = textlatitude.text.toString()
            val registration: String = vehicleregistration.text.toString()
            val id = fuelType.checkedRadioButtonId
            var radioSelect = 0
            if (id == 2131296473) {
                radioSelect = 1
            } else if (id == 2131296474) {
                radioSelect = 0
            }
            Log.d("TAG-ID", id.toString())
            Log.d("TAG-RADIO", radioSelect.toString())

            addingFunction(
                prefUserId,
                odometr,
                registration,
                radioSelect,
                fuel,
                addlatitude,
                addlongitude
            )
        }

        btnBack.setOnClickListener {
            super.onBackPressed()
        }
    }

    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if ((ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                locationPermissionCode
            )
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)
    }

    override fun onLocationChanged(location: Location) {
        val df = DecimalFormat("##.###")
        val gpsLocation =
            "N" + df.format(location.latitude) + " - E" + df.format(location.longitude)
        val latitude = df.format(location.latitude)
        val longitude = df.format(location.longitude)

        val textViewLocation = findViewById<TextView>(R.id.fuel_localisation)
        val textlongitude = findViewById<TextView>(R.id.latitude)
        val textlatitude = findViewById<TextView>(R.id.longitude)

        textViewLocation.text = (gpsLocation)
        textlatitude.text = (latitude)
        textlongitude.text = (longitude)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED
        ) {

            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkLoginSessin() {
        val checkloginsessionifok = SharedPref().init(applicationContext)
        if (checkloginsessionifok == 1) {
            Log.d("TAG==login session", checkloginsessionifok.toString())
        } else {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }


    private fun addingFunction(
        prefUserId: String?,
        Odometr: String,
        registration: String,
        radioSelect: Int,
        fuel: String,
        latitude: String,
        longitude: String
    ) {
        val response = khttp.post(
            url = "https://ekolives.com/app-rest/addgasoil.php",
            data = mapOf(
                "userid" to prefUserId,
                "registration" to registration,
                "km" to Odometr,
                "type" to radioSelect,
                "volume" to fuel,
                "latitude" to latitude,
                "longitude" to longitude,
                "secret" to "dupa2"
            )
        )


        when (response.text.replace(" ", "")) {
            "1" -> {
                val responsetxt = getString(R.string.addSusscess)
                Toast.makeText(this@FuelActivity, responsetxt, Toast.LENGTH_LONG).show()
                super.onBackPressed()
            }
            "2" -> {
                val responsetxt = getString(R.string.dbError)
                Toast.makeText(this@FuelActivity, responsetxt, Toast.LENGTH_LONG).show()
            }
            "3" -> {
                val responsetxt = getString(R.string.addError)
                Toast.makeText(this@FuelActivity, responsetxt, Toast.LENGTH_LONG).show()
            }
        }
    }

}







