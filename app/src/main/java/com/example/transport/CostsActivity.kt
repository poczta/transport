package com.example.transport

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.*

class CostsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_costs)

        val costs = resources.getStringArray(R.array.costs)
        val spinner = findViewById<Spinner>(R.id.costsSpinner)
        val costsAmount = findViewById<EditText>(R.id.costsAmount)
        val btnBack = findViewById<Button>(R.id.btn_back)
        val btnPhoto = findViewById<Button>(R.id.button_take_picture)
        val btnSubmit = findViewById<Button>(R.id.btn_submit)
        val pref = getSharedPreferences("UserSessionLogin", Context.MODE_PRIVATE)
        val prefUserId = pref.getString("UserId", "")

        btnBack.setOnClickListener {
            super.onBackPressed()
        }

        btnPhoto.setOnClickListener {
            openCameraInterface()
        }



        btnSubmit.setOnClickListener {
            val spinpos = spinner.selectedItemPosition
            val spinposready = spinpos + 1
            val coststxt: String = costsAmount.text.toString()

            Log.d("TAG==cost amount", coststxt)
            addingFunction(coststxt, spinposready, prefUserId)
        }


        if (spinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, costs
            )
            spinner.adapter = adapter
        }
    }


    private val IMAGE_CAPTURE_CODE = 1001
    private var imageUri: Uri? = null
    private fun openCameraInterface() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, R.string.takepicture)
        values.put(MediaStore.Images.Media.DESCRIPTION, R.string.take_picture_description)
        imageUri = contentResolver?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)// Create camera intent
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)// Launch intent
        startActivityForResult(intent, IMAGE_CAPTURE_CODE)
    }



    private fun addingFunction(coststxt: String, spinposready: Int, prefUserId: String?) {
        val response = khttp.post(
            url = "https://ekolives.com/app-rest/addcosts.php",
            data = mapOf(
                "userid" to prefUserId,
                "type" to spinposready,
                "cost" to coststxt,
                "currency" to "PLN",
                "secret" to "dupa1"
            )
        )

        when (response.text) {
            "1" -> {
                val responsetxt = getString(R.string.addSusscess)
                Toast.makeText(this@CostsActivity, responsetxt, Toast.LENGTH_LONG).show()
                super.onBackPressed()
            }
            "2" -> {
                val responsetxt = getString(R.string.dbError)
                Toast.makeText(this@CostsActivity, responsetxt, Toast.LENGTH_LONG).show()
            }
            "3" -> {
                val responsetxt = getString(R.string.addError)
                Toast.makeText(this@CostsActivity, responsetxt, Toast.LENGTH_LONG).show()
            }
        }

    }
}


